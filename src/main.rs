use std::{i32, fs::File, io::Write};
use linfa_trees::{DecisionTree, SplitQuality};
use ndarray::Array2;
use linfa::prelude::*;
use ndarray::prelude::*;

fn main() {
    let original_data: Array2<f32> = array![
        [1., 1. , 1000., 1., 10.],
        [1., 0. , 0., 1., 6.],
        [1., 0. , 0., 1., 6.],
        [1., 0. , 0., 1., 6.],
        [1., 0. , 0., 1., 6.],
        [1., 1. , 800., 1., 8.],
        [1., 0. , 0., 0., 0.],
        [1., 1. , 0., 1., 9.],
        [1., 1. , 0., 1., 8.],
        [1., 1. , 800., 1., 8.],
        [1., 1. , 0., 1., 8.],
        [1., 1. , 500., 0., 8.],
        [1., 0. , 50., 0., 3.],
        [1., 1. , 50., 0., 4.],
        [1., 0. , 50., 0., 3.],
    ];

    let feature_names = vec!["Watched TV", "Pet Cat", "Rust Lines", "Eat Pizza"];

    let number_of_features = original_data.len_of(Axis(1)) - 1;
    let features = original_data.slice(s![.., 0..number_of_features]).to_owned();
    let labels = original_data.column(number_of_features).to_owned();

    let linfa_dataset = Dataset::new(features, labels).
    map_targets(|x| match x.to_owned() as i32{
        i32::MIN..=4 => "SAD",
        5..=7 => "OK",
        8..=i32::MAX => "HAPPY"
    });

    let model = DecisionTree::params()
        .split_quality(SplitQuality::Gini)
        .fit(&linfa_dataset)
        .unwrap();

    File::create("dt.tex")
    .unwrap()
    .write_all(model.export_to_tikz().with_legend().to_string().as_bytes())
    .unwrap();
}
